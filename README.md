# Pokèmon Tournament

## Startup the project:

Move into the project root and run the following command 

```docker-compose up --build -d``` 

this will start the project and it will run in background

When the services are online we can install the dependencies running this command:

```composer install```

After that we can install front-end dependencies running this command

```npm install ``` 

or simplified 

```npm i```

Now we have all the dependencies, but we have to compile our assets, so we have tu run the following command: 

```npm run dev```

When everything is done we can create the database tables running this command: 

```php bin/console doctrine:migrations:migrate```

Now we are ready, we can go at [Pokemon Tournament page](http://pokemon.test/)