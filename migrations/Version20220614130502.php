<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220614130502 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE abilities (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(100) NOT NULL, hidden TINYINT(1) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE abilities_pokemon (abilities_id INT NOT NULL, pokemon_id INT NOT NULL, INDEX IDX_19CCD5DF1E1F6EAC (abilities_id), INDEX IDX_19CCD5DF2FE71C3E (pokemon_id), PRIMARY KEY(abilities_id, pokemon_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE pokemon (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(100) NOT NULL, name_slug VARCHAR(255) NOT NULL, base_experience BIGINT NOT NULL, UNIQUE INDEX UNIQ_62DC90F3DF2B4115 (name_slug), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE pokemon_team (pokemon_id INT NOT NULL, team_id INT NOT NULL, INDEX IDX_F849D85C2FE71C3E (pokemon_id), INDEX IDX_F849D85C296CD8AE (team_id), PRIMARY KEY(pokemon_id, team_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE sprite (id INT AUTO_INCREMENT NOT NULL, pokemon_id INT DEFAULT NULL, url VARCHAR(255) DEFAULT NULL, UNIQUE INDEX UNIQ_351D8F9E2FE71C3E (pokemon_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE team (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(100) NOT NULL, creation_date DATETIME DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE types (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(100) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE types_pokemon (types_id INT NOT NULL, pokemon_id INT NOT NULL, INDEX IDX_BFAE27C58EB23357 (types_id), INDEX IDX_BFAE27C52FE71C3E (pokemon_id), PRIMARY KEY(types_id, pokemon_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE messenger_messages (id BIGINT AUTO_INCREMENT NOT NULL, body LONGTEXT NOT NULL, headers LONGTEXT NOT NULL, queue_name VARCHAR(255) NOT NULL, created_at DATETIME NOT NULL, available_at DATETIME NOT NULL, delivered_at DATETIME DEFAULT NULL, INDEX IDX_75EA56E016BA31DB (delivered_at), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE abilities_pokemon ADD CONSTRAINT FK_19CCD5DF1E1F6EAC FOREIGN KEY (abilities_id) REFERENCES abilities (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE abilities_pokemon ADD CONSTRAINT FK_19CCD5DF2FE71C3E FOREIGN KEY (pokemon_id) REFERENCES pokemon (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE pokemon_team ADD CONSTRAINT FK_F849D85C2FE71C3E FOREIGN KEY (pokemon_id) REFERENCES pokemon (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE pokemon_team ADD CONSTRAINT FK_F849D85C296CD8AE FOREIGN KEY (team_id) REFERENCES team (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE sprite ADD CONSTRAINT FK_351D8F9E2FE71C3E FOREIGN KEY (pokemon_id) REFERENCES pokemon (id)');
        $this->addSql('ALTER TABLE types_pokemon ADD CONSTRAINT FK_BFAE27C58EB23357 FOREIGN KEY (types_id) REFERENCES types (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE types_pokemon ADD CONSTRAINT FK_BFAE27C52FE71C3E FOREIGN KEY (pokemon_id) REFERENCES pokemon (id) ON DELETE CASCADE');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE abilities_pokemon DROP FOREIGN KEY FK_19CCD5DF1E1F6EAC');
        $this->addSql('ALTER TABLE abilities_pokemon DROP FOREIGN KEY FK_19CCD5DF2FE71C3E');
        $this->addSql('ALTER TABLE pokemon_team DROP FOREIGN KEY FK_F849D85C2FE71C3E');
        $this->addSql('ALTER TABLE sprite DROP FOREIGN KEY FK_351D8F9E2FE71C3E');
        $this->addSql('ALTER TABLE types_pokemon DROP FOREIGN KEY FK_BFAE27C52FE71C3E');
        $this->addSql('ALTER TABLE pokemon_team DROP FOREIGN KEY FK_F849D85C296CD8AE');
        $this->addSql('ALTER TABLE types_pokemon DROP FOREIGN KEY FK_BFAE27C58EB23357');
        $this->addSql('DROP TABLE abilities');
        $this->addSql('DROP TABLE abilities_pokemon');
        $this->addSql('DROP TABLE pokemon');
        $this->addSql('DROP TABLE pokemon_team');
        $this->addSql('DROP TABLE sprite');
        $this->addSql('DROP TABLE team');
        $this->addSql('DROP TABLE types');
        $this->addSql('DROP TABLE types_pokemon');
        $this->addSql('DROP TABLE messenger_messages');
    }
}
