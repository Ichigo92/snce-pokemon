<?php

namespace App\Controller;

use App\Entity\Team;
use App\Exception\TeamNameValidationException;
use App\Services\TeamService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class TeamController extends AbstractController
{
    #[Route("/team/create", name: "team_create")]
    #[Route("/team/create/{id}", name: "team_created")]
    public function index(int $id = null, TeamService $teamService): Response
    {
        return $this->render('team/index.html.twig', [
            'title' => 'Create Team',
            'team' => $id ? $teamService->getTeam($id) : null,
            'pokemons' => $id ? $teamService->getPokemons($id) : []
        ]);
    }

    #[Route("/team/add", name: "team_add", methods: 'POST')]
    public function createTeam(Request $request, TeamService $teamService): JsonResponse
    {
        $teamName = $request->get('name');

        try {
            $team = $teamService->createTeam($teamName);
        }catch (\Exception $e) {
            return new JsonResponse([
                'message' => $e->getMessage()
            ], Response::HTTP_BAD_REQUEST);
        }

        return new JsonResponse([
            'team_name' => $team->getName(),
            'team_id' => $team->getId()
        ], Response::HTTP_CREATED);
    }

    #[Route("/team/{teamId}/add-pokemon", name: "team_add_pokemon")]
    public function addPokemon(int $teamId, TeamService $teamService): JsonResponse
    {
        try {
            $pokemon = $teamService->addPokemonToTeam($teamId);
        }catch (\Exception $e) {
            return new JsonResponse([
                'message' => $e->getMessage()
            ], Response::HTTP_BAD_REQUEST);
        }

        return new JsonResponse([
            'pokemon' => $pokemon->getName()
        ], Response::HTTP_OK);
    }

    #[Route("/team/list", name: "team_list")]
    public function teamList(TeamService $teamService): Response
    {
        $list = $teamService->getAllTeams();

        return $this->render('team/list.html.twig', [
            'title' => 'Team List',
            'list' => $list
        ]);
    }

    #[Route("/team/{teamId}/edit", name: "team_edit")]
    public function editTeam(int $teamId, TeamService $teamService)
    {
        /**
         *@var Team $team
        */
        $team = $teamService->getTeam($teamId);

        return $this->render('team/edit.html.twig', [
            'title' => "Edit team {$team->getName()}",
            'team' => $team,
            'pokemons' => $team->getPokemon()
        ]);
    }

    #[Route("/team/{teamId}/edit/remove-pokemon/{pokemonId}", name: "team_remove_pokemon", methods: 'POST')]
    public function teamRemovePokemon(int $teamId, int $pokemonId, TeamService $teamService): JsonResponse
    {
        try {
            $teamService->removePokemon($teamId, $pokemonId);
        }catch (\Exception $e) {
            return new JsonResponse([
                'message' => $e->getMessage()
            ], Response::HTTP_BAD_REQUEST);
        }

        return new JsonResponse([], Response::HTTP_OK);
    }

    #[Route("/team/{teamId}/edit/change-name", name: "team_change_name", methods: 'POST')]
    public function changeTeamName(int $teamId, TeamService $teamService, Request $request): JsonResponse
    {
        try {
            $teamService->changeTeamName($teamId, $request->get('team_name'));
        }catch (TeamNameValidationException $e) {
            return new JsonResponse([
                'message' => $e->getMessage()
            ], Response::HTTP_BAD_REQUEST);
        }

        return new JsonResponse([], Response::HTTP_OK);
    }
}
