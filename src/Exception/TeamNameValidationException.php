<?php

namespace App\Exception;

use App\Constant\Exception\Team;
use Throwable;

class TeamNameValidationException extends \Exception
{
    public function __construct(string $message = null, int $code = 0, ?Throwable $previous = null)
    {

        parent::__construct($message ?? Team::TEAM_NAME_EXCEPTION, $code, $previous);
    }
}