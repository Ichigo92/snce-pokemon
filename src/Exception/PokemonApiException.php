<?php

namespace App\Exception;

use App\Constant\Exception\Api;

class PokemonApiException extends \Exception
{
    public function __construct(string $message = null, int $code = 0, ?Throwable $previous = null)
    {
        parent::__construct($message ?? Api::POKEMON_API_EXCEPTION, $code, $previous);
    }
}