<?php

namespace App\Exception;

use App\Constant\Exception\Team;

class TeamExceedException extends \Exception
{
    public function __construct(string $message = null, int $code = 0, ?Throwable $previous = null)
    {
        parent::__construct($message ?? Team::TEAM_EXCEED_EXCEPTION, $code, $previous);
    }
}