<?php

namespace App\Constant\Exception;

class Team
{
    const TEAM_NAME_EXCEPTION = "<b>Invalid team name</b><hr>The available character are: <ul><li>Upper case words</li><li>Lower case words</li><li>Spaces</li><li>-</li><li>Max length 100 characters</li></ul>";
    const TEAM_EXCEED_EXCEPTION = "You already have the maximum number of Pokemon in your team (6)! ";

}