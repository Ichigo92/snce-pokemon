<?php

namespace App\Constant\Exception;

class Api
{
    const POKEMON_API_EXCEPTION = "An error occurred while retrieving pokemon data, try again";

}