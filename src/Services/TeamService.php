<?php

namespace App\Services;

use App\Entity\Pokemon;
use App\Entity\Team;
use App\Exception\TeamExceedException;
use App\Exception\TeamNameValidationException;
use Doctrine\Persistence\ManagerRegistry;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\Cache\Adapter\FilesystemAdapter;
use Symfony\Contracts\Cache\ItemInterface;

class TeamService
{
    private ObjectManager $em;

    private FilesystemAdapter $cache;

    private PokemonService $pokemonService;

    public function __construct(ManagerRegistry $doctrine, PokemonService $pokemonService)
    {
        $this->em = $doctrine->getManager();
        $this->pokemonService = $pokemonService;
        $this->cache = new FilesystemAdapter();
    }

    /**
     * @throws TeamNameValidationException
     */
    public function createTeam(string $teamName = null): Team
    {
        $this->validateTeamName($teamName);
        $team = new Team($teamName);
        $this->em->persist($team);
        $this->em->flush();
        $this->clearTeamCache();

        return $team;
    }

    public function getTeam(int $id)
    {
        $repo = $this->em->getRepository(Team::class);
        return $repo->find($id);
    }

    public function getPokemons(int $teamId)
    {
        $repo = $this->em->getRepository(Team::class);
        /**
         *@var Team $team
        */
        $team = $repo->find($teamId);
        return $team->getPokemon()->toArray();
    }

    /**
     * @throws TeamExceedException
     */
    public function addPokemonToTeam(int $teamId): Pokemon
    {
        $teamRepo = $this->em->getRepository(Team::class);
        /**
         *@var Team $team
         */
        $team = $teamRepo->find($teamId);

        if (count($team->getPokemon()->toArray()) >= 6) {
            throw new TeamExceedException();
        }

        $pokemon = $this->pokemonService->getPokemon();
        $pokemon->addTeam($team);
        $this->em->persist($pokemon);
        $this->em->flush();
        $this->clearTeamCache();

        return $pokemon;
    }

    public function getAllTeams()
    {
        //$this->clearTeamCache();
        $repo = $this->em->getRepository(Team::class);

        $result = $this->cache->get('team.list', function (ItemInterface $item) use ($repo) {
            //cache expires after 1 hour
            $item->expiresAfter(3600);

            $result = [];
            /**
             * @var Team $team
             */
            $teams = $repo->findAll();
            foreach ($teams as $team) {
                $teamTypes = [];

                $result[$team->getId()] = [
                    'team' => $team,
                    'pokemon' => $team->getPokemon()->getValues(),
                ];

                foreach ($team->getPokemon()->getValues() as $pokemon) {
                    foreach ($pokemon->getTypes()->getValues() as $type) {
                        $teamTypes[] = $type->getName();
                    }
                }
                $result[$team->getId()]['types'] = array_unique($teamTypes);
            }

            return $result;
        });


        return $result;
    }

    public function removePokemon(int $teamId, int $pokemonId): void
    {
        $teamRepo = $this->em->getRepository(Team::class);
        $pokemonRepo = $this->em->getRepository(Pokemon::class);

        /**
         *@var Team $team
         */
        $team = $teamRepo->find($teamId);

        /**
         *@var Pokemon $pokemon
        */
        $pokemon = $pokemonRepo->find($pokemonId);

        $pokemon->removeTeam($team);

        $this->em->persist($pokemon);
        $this->em->flush();
        $this->clearTeamCache();
    }

    /**
     * @throws TeamNameValidationException
     */
    public function changeTeamName(int $teamId, string $teamName): void
    {
        $teamRepo = $this->em->getRepository(Team::class);
        /**
         *@var Team $team
         */
        $team = $teamRepo->find($teamId);

        $this->validateTeamName($teamName);

        $team->setName($teamName);
        $this->em->persist($team);
        $this->em->flush();
        $this->clearTeamCache();
    }

    /**
     * @throws TeamNameValidationException
     */
    private function validateTeamName(string $teamName = null): void
    {
        if (!$teamName || !preg_match('/^[A-Za-z\s-]{0,100}$/', $teamName, $matches)) {
            throw new TeamNameValidationException();
        }
    }

    private function clearTeamCache(): void
    {
        $this->cache->delete('team.list');
    }

}