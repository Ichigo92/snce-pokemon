<?php

namespace App\Services;

use App\Entity\Abilities;
use App\Entity\Pokemon;
use App\Entity\Sprite;
use App\Entity\Types;
use App\Exception\PokemonApiException;
use App\Repository\PokemonRepository;
use Doctrine\Persistence\ManagerRegistry;
use Doctrine\Persistence\ObjectManager;
use PokePHP\PokeApi;
use Symfony\Component\String\Slugger\AsciiSlugger;

class PokemonService
{
    private PokeApi $pokeApi;

    private ObjectManager $em;

    public function __construct(PokeApi $pokeApi, ManagerRegistry $doctrine)
    {
        $this->pokeApi = $pokeApi;
        $this->em = $doctrine->getManager();
    }

    public function getPokemon(): Pokemon
    {
        return $this->getRandomPokemon();
    }

    /**
     * @throws PokemonApiException
     */
    private function getRandomPokemon(): Pokemon
    {
        $randomNumber = rand(1, 1126);
        $slugger = new AsciiSlugger();

        $pokemonApi = json_decode($this->pokeApi->pokemon($randomNumber), true);

        $repo = $this->em->getRepository(Pokemon::class);

        if (!is_array($pokemonApi)) {
            throw new PokemonApiException();
        }

        $storedPokemon = $repo->findOneBy(['name_slug' => $pokemonApi['name']]);

        if (!$storedPokemon) {
            $pokemon = new Pokemon();
            $pokemon->setName(ucwords($pokemonApi['name']));
            $pokemon->setBaseExperience($pokemonApi['base_experience']);

            $pokemon->setNameSlug($slugger->slug($pokemonApi['name']));
            $this->em->persist($pokemon);
            $this->em->flush();

            $sprite = new Sprite();
            $sprite->setPokemon($pokemon);
            $sprite->setUrl($pokemonApi['sprites']['front_default']);
            $pokemon->setSprite($sprite);
            $this->em->persist($sprite);
            $this->em->persist($pokemon);

            foreach ($pokemonApi['abilities'] as $pokemonAbility) {
                $ability = new Abilities();
                $ability->setName(ucwords($pokemonAbility['ability']['name']));
                $ability->setHidden($pokemonAbility['is_hidden']);
                $ability->addPokemon($pokemon);
                $this->em->persist($ability);
            }

            foreach ($pokemonApi['types'] as $pokemonType) {
                $type = new Types();
                $type->setName(ucwords($pokemonType['type']['name']));
                $type->addPokemon($pokemon);
                $this->em->persist($type);
            }

            $this->em->flush();


            return $pokemon;
        }


        return $storedPokemon;
    }
    
}