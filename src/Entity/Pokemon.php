<?php

namespace App\Entity;

use App\Repository\PokemonRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: PokemonRepository::class)]
#[ORM\Table(name: 'pokemon')]
class Pokemon
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private $id;

    #[ORM\ManyToMany(targetEntity: Team::class, inversedBy: 'pokemon')]
    private $team;

    #[ORM\Column(type: 'string', length: 100)]
    private $name;

    #[ORM\Column(type: 'string', length: 255, unique: true)]
    private $name_slug;

    #[ORM\Column(type: 'bigint')]
    private $base_experience;

    #[ORM\OneToOne(mappedBy: 'pokemon', targetEntity: Sprite::class, cascade: ['persist', 'remove'])]
    private $sprite;

    #[ORM\ManyToMany(targetEntity: Abilities::class, mappedBy: 'pokemon')]
    private $abilities;

    #[ORM\ManyToMany(targetEntity: Types::class, mappedBy: 'pokemon')]
    private $types;

    public function __construct()
    {
        $this->team = new ArrayCollection();
        $this->abilities = new ArrayCollection();
        $this->types = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return Collection<int, Team>
     */
    public function getTeam(): Collection
    {
        return $this->team;
    }

    public function addTeam(Team $team): self
    {
        if (!$this->team->contains($team)) {
            $this->team[] = $team;
        }

        return $this;
    }

    public function removeTeam(Team $team): self
    {
        $this->team->removeElement($team);

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getNameSlug(): ?string
    {
        return $this->name_slug;
    }

    public function setNameSlug(string $name_slug): self
    {
        $this->name_slug = $name_slug;

        return $this;
    }

    public function getBaseExperience(): ?string
    {
        return $this->base_experience;
    }

    public function setBaseExperience(string $base_experience): self
    {
        $this->base_experience = $base_experience;

        return $this;
    }

    public function getSprite(): ?Sprite
    {
        return $this->sprite;
    }

    public function setSprite(?Sprite $sprite): self
    {
        // unset the owning side of the relation if necessary
        if ($sprite === null && $this->sprite !== null) {
            $this->sprite->setPokemon(null);
        }

        // set the owning side of the relation if necessary
        if ($sprite !== null && $sprite->getPokemon() !== $this) {
            $sprite->setPokemon($this);
        }

        $this->sprite = $sprite;

        return $this;
    }

    /**
     * @return Collection<int, Abilities>
     */
    public function getAbilities(): Collection
    {
        return $this->abilities;
    }

    public function addAbility(Abilities $ability): self
    {
        if (!$this->abilities->contains($ability)) {
            $this->abilities[] = $ability;
            $ability->addPokemon($this);
        }

        return $this;
    }

    public function removeAbility(Abilities $ability): self
    {
        if ($this->abilities->removeElement($ability)) {
            $ability->removePokemon($this);
        }

        return $this;
    }

    /**
     * @return Collection<int, Types>
     */
    public function getTypes(): Collection
    {
        return $this->types;
    }

    public function addType(Types $type): self
    {
        if (!$this->types->contains($type)) {
            $this->types[] = $type;
            $type->addPokemon($this);
        }

        return $this;
    }

    public function removeType(Types $type): self
    {
        if ($this->types->removeElement($type)) {
            $type->removePokemon($this);
        }

        return $this;
    }
}
