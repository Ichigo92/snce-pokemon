#!/bin/sh

set -e

role=${CONTAINER_ROLE:-app}

if [ "${COMPOSER_INSTALL}" == "1" ]; then
    composer install -o
    composer dumpautoload -o
fi

php-fpm "$@"
